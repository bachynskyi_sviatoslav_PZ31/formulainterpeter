﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using FormulaInterpreter.Core;

namespace FormulaInterpreter.Demonstration
{
    public partial class Execution : Form
    {
        public Execution(Code code)
        {
            InitializeComponent();
            if (!this.IsHandleCreated)
                this.CreateHandle();
            code.onWrite += new EventHandler<WriteArgs>(onWrite);
            code.onRead += new EventHandler<ReadArgs>(onRead);
            new Task(code.Execute).Start();
        }
        void onWrite(object sender, WriteArgs writeArgs)
        {
            this.Invoke((Action)(() =>
            {
                if (writeArgs.Char == '\n')
                    tbOutput.Text += Environment.NewLine;
                else
                    tbOutput.Text += writeArgs.Char;
            }));
        }
        Buffer buffer = new Buffer();
        void onRead(object sender, ReadArgs readArgs)
        {
           readArgs.Char = buffer.Read();
        }
        private void tbInput_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.KeyCode == Keys.Enter)
            {
                string inputedValue = tbInput.Text + Environment.NewLine;
                buffer.Write(inputedValue);
                tbOutput.Text += inputedValue;
                tbInput.Text = "";
            }
        }
    }
}
