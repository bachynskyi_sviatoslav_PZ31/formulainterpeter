﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using FormulaInterpreter.Core;

namespace FormulaInterpreter.Demonstration
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }
        private bool CloseFile()
        {
            if (newFile && rtbInput.TextLength == 0) return true;
            DialogResult result =
            MessageBox.Show("Save changes?", "Warning", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning);
            if (result == DialogResult.Cancel) return false;
            if (result == DialogResult.No) return true;
            return SaveFile();
        }
        private void ExitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (CloseFile())
                Close();
        }
        string fileName = null;
        private void OpenToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (CloseFile() && openFileDialog.ShowDialog() == DialogResult.OK)
            {
                using (StreamReader reader = new StreamReader(openFileDialog.FileName))
                {
                    fileName = openFileDialog.FileName;
                    var s = reader.ReadToEnd();
                    rtbInput.Text = s;
                    newFile = false;
                }
            }
        }
        private bool SaveFile()
        {
            if (fileName == null)
            {
                if (saveFileDialog.ShowDialog() != DialogResult.OK) return false;
                fileName = saveFileDialog.FileName;
            }
            using (StreamWriter writer = new StreamWriter(fileName, false, Encoding.UTF8))
            {
                writer.Write(rtbInput.Text.Replace("\n", Environment.NewLine));
                return true;
            }
        }
        private void SaveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveFile();
            newFile = false;
        }
        bool newFile = true;
        private void NewToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (CloseFile())
            {
                rtbInput.Text = "";
                fileName = null;
                newFile = true;
            }
        }
        private void MenuStrip_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }
        private void MainForm_Load(object sender, EventArgs e)
        {
        }
        List<Token> LexerErrorHandling(string code)
        {
            try
            {
            return Lexer.Analyse(rtbInput.Text);
            }
            catch (LexerException ex)
            {
                MessageBox.Show(ex.Message);
                return null;
            }
        }
        List<AST> ParserErrorHandling(string code)
        {
            List<Token> tokens = LexerErrorHandling(code);
            if (tokens == null) return null;
            try
            {
                return Parser.Analyse(tokens);
            }
            catch (ParserException ex)
            {
                MessageBox.Show(ex.Message);
                return null;
            }
        }
        Scope SemanterErrorHandling(string code)
        {
            List<AST> asts = ParserErrorHandling(code);
            if (asts == null) return null;
            try
            {
                return Semanter.Analyze(asts);
            }
            catch (SemanterException ex)
            {
                MessageBox.Show(ex.Message);
                return null;
            }
        }
        private void LexerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            StringBuilder result = new StringBuilder();
            List<Token> tokens = LexerErrorHandling(rtbInput.Text);
            if (tokens == null) return;
            foreach (Token token in tokens)
            {
                result.Append("<");
                result.Append(token.Type);
                result.Append("> - \"");
                result.Append(token.Lexem);
                result.Append("\" :");
                result.AppendLine(token.LineNumber.ToString());
            }
            ResultsForm lexerResult = new ResultsForm(result.ToString());
            lexerResult.ShowDialog();
        }

        private void ParserToolStripMenuItem_Click(object sender, EventArgs e)
        {
            List<AST> ASTs = ParserErrorHandling(rtbInput.Text);
        }
        private void SemanterToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Scope scope = SemanterErrorHandling(rtbInput.Text);
        }
        private void CodeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Scope scope = SemanterErrorHandling(rtbInput.Text);
            if (scope == null) return;
            Code code = CodeGenarator.Run(scope);
            var execution = new Execution(code);
            execution.Show();
        }
    }
}
