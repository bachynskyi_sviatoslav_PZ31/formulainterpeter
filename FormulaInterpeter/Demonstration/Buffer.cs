﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace FormulaInterpreter.Demonstration
{
    class Buffer
    {
        object lockObject = new object();
        StringBuilder buffer = new StringBuilder();
        int readStart = 0;
        public Buffer()
        {
        }
        public char Read()
        {
            lock (lockObject)
            {
                while (readStart == buffer.Length)
                {
                    Monitor.Wait(lockObject);
                }
                return buffer[readStart++];
            }
        }
        public void Write(char value)
        {
            lock (lockObject)
            {
                buffer.Append(value);
                Monitor.Pulse(lockObject);
            }
        }
        public void Write(string str)
        {
            foreach(var ch in str)
            {
                Write(ch);
            }
        }
    }
}
