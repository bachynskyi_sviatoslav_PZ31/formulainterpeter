﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using FormulaInterpreter.Core;

namespace FormulaInterpreter.UnitTests
{
    [TestClass]
    public class LexerUnitTests
    {
        private void AssertTokensAreEqual(List<Token> fst, List<Token> snd)
        {
            Assert.AreEqual(fst.Count, snd.Count);
            for (int i = 0; i < fst.Count; i++)
            {
                Assert.AreEqual(fst[i], snd[i]);
            }
        }
        [TestMethod]
        public void Analyse_WrongToken_ThrowsException()
        {
            string input = "real sth(real num)\n" +
                "{ result = ' @s{}#@)@#";
            Assert.ThrowsException<InvalidTokenException>(
                () => Lexer.Analyse(input));
        }
        [TestMethod]
        public void Analyse_AFewComment_Returns0Tokens()
        {
            string input = "/* fst comment *//* snd comment *///trd comment";
            List<Token> result = Lexer.Analyse(input);
            Assert.AreEqual(result.Count, 0);
        }
        [TestMethod]
        public void Analyse_UsualData_GeneratesTokens()
        {
            string input = "real Add(real x,real y){\n" +
                "result = x+y;\n" +
                "}\n" +
                "real a = 1, b = 3.5;\n" +
                "out Add(a,b);\n";
            List<Token> expectedResult = new List<Token>()
            {
                new Token(TokenType.Type, "real",1),
                new Token(TokenType.Identifier,"Add",1),
                new Token(TokenType.Bracket,"(",1),
                new Token(TokenType.Type,"real",1),
                new Token(TokenType.Identifier,"x",1),
                new Token(TokenType.Spliter,",",1),
                new Token(TokenType.Type,"real",1),
                new Token(TokenType.Identifier,"y",1),
                new Token(TokenType.Bracket,")",1),
                new Token(TokenType.Bracket,"{",1),
                new Token(TokenType.Keyword,"result",2),
                new Token(TokenType.Operation,"=",2),
                new Token(TokenType.Identifier,"x",2),
                new Token(TokenType.Operation,"+",2),
                new Token(TokenType.Identifier,"y",2),
                new Token(TokenType.Statement,";",2),
                new Token(TokenType.Bracket,"}",3),
                new Token(TokenType.Type,"real",4),
                new Token(TokenType.Identifier,"a",4),
                new Token(TokenType.Operation,"=",4),
                new Token(TokenType.IntegerLiteral,"1",4),
                new Token(TokenType.Spliter,",",4),
                new Token(TokenType.Identifier,"b",4),
                new Token(TokenType.Operation,"=",4),
                new Token(TokenType.RealLiteral,"3.5",4),
                new Token(TokenType.Statement,";",4),
                new Token(TokenType.Keyword,"out",5),
                new Token(TokenType.Identifier,"Add",5),
                new Token(TokenType.Bracket,"(",5),
                new Token(TokenType.Identifier,"a",5),
                new Token(TokenType.Spliter,",",5),
                new Token(TokenType.Identifier,"b",5),
                new Token(TokenType.Bracket,")",5),
                new Token(TokenType.Statement,";",5)
            };
            List<Token> result = Lexer.Analyse(input);
            AssertTokensAreEqual(result, expectedResult);
        }
    }
}
