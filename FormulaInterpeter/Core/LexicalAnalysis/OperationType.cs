﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
// the idea of dividing binary operations came after half of the work was done
namespace FormulaInterpreter.Core
{
    static class OperationType
    {
        public static bool IsEqualifier(Token token)
        {
            return (token.Type == TokenType.Operation && (token.Lexem == "!=" || token.Lexem == "=="));
        }
        public static bool IsComparier(Token token)
        {
            return (token.Type == TokenType.Operation && (token.Lexem == ">=" || token.Lexem == ">" || token.Lexem == "<" || token.Lexem == "<="));
        }
        public static bool IsRelational(Token token)
        {
            return IsEqualifier(token) || IsComparier(token);
        }
        public static bool IsTerm(Token token)
        {
            return (token.Type == TokenType.Operation && (token.Lexem == "+" || token.Lexem == "-"));
        }
        public static bool IsMultiplier(Token token)
        {
            return (token.Type == TokenType.Operation && (token.Lexem == "*" || token.Lexem == "/"));
        }
        public static bool IsArithmetical(Token token)
        {
            return IsTerm(token) || IsMultiplier(token);
        }
    }
}
