﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace FormulaInterpreter.Core
{
    public class Pattern
    {
        public TokenType TokenType { get; private set; }
        private Regex regex { get; set; }
        public Pattern(TokenType tokenType, Regex regex)
        {
            TokenType = tokenType;
            this.regex = regex;
        }
        public Match Match { get; private set; }
        public bool IsMatch(string text, int startat)
        {
            Match = regex.Match(text, startat);
            return Match.Success;
        }
        public Token CreateToken(int lineNumber)
        {
            if (!Match.Success) throw new Exception("Pattern doesn't match");
            return new Token(TokenType,Match.Value,lineNumber);
        }
    }
}