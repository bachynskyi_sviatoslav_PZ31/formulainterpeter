﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace FormulaInterpreter.Core
{
    public static class Lexer
    {
        public static List<Token> Analyse(string text)
        {
            List<Token> result = new List<Token>();
            List<Regex> patternsToSkip = new List<Regex>()
            {
                new Regex(@"\G[ \t\n]+"), //whitespaces
                new Regex(@"\G\/\*.*?(\*\/)", RegexOptions.Singleline), /*multi line comments */
                new Regex(@"\G//.*\n?") //single line comments
        };
            Regex newLinesInside = new Regex(@"\n+");
            List<Pattern> patterns = new List<Pattern>()
            { 
                new Pattern(TokenType.Bracket, new Regex(@"\G({|}|\(|\))")),
                new Pattern(TokenType.Operation, new Regex(@"\G(\+|-|\*|\/|>=|<=|<|>|==|!=|=)")), //TODO %
                new Pattern(TokenType.Keyword, new Regex(@"\G(if|else|in|out|while)\b")),
                new Pattern(TokenType.Type, new Regex(@"\G(real|integer|string|boolean)\b")),
                new Pattern(TokenType.BooleanLiteral, new Regex(@"\G(true|false)\b")),
                new Pattern(TokenType.Identifier, new Regex(@"\G([A-Z]|[a-z])\w*\b")),
                new Pattern(TokenType.Statement, new Regex(@"\G;")),
                new Pattern(TokenType.Spliter, new Regex(@"\G,")),
                new Pattern(TokenType.RealLiteral, new Regex(@"\G\d+(\.\d+)")),
                new Pattern(TokenType.IntegerLiteral, new Regex(@"\G\d+")),
                new Pattern(TokenType.StringLiteral, new Regex(@"\G"".*?""",
                RegexOptions.Singleline)),
            };
            int startIndex = 0; //current position
            int lineNumber = 1; //current line
            while (startIndex < text.Length)
            {
                bool skippedAnyPattern = false;
                foreach (var pattern in patternsToSkip)
                {
                    Match match = pattern.Match(text, startIndex);
                    if (match.Success)
                    {
                        startIndex += match.Length;
                        //count newline symbols inside multiline comment
                        MatchCollection newLinesInsideMatches =
                            newLinesInside.Matches(match.Value);
                        foreach (Match newLinesInsideMatch in newLinesInsideMatches)
                        {
                            lineNumber += newLinesInsideMatch.Length;
                        }
                        skippedAnyPattern = true;
                    }
                }
                if (skippedAnyPattern) continue;//go to previous patterns
                bool breakHappend = false;
                foreach (var pattern in patterns)
                {
                    if (pattern.IsMatch(text, startIndex))
                    {
                        startIndex += pattern.Match.Length;
                        result.Add(pattern.CreateToken(lineNumber));
                        breakHappend = true;
                        break; //go to previous patterns
                    }
                }
                if (!breakHappend) //throw InvalidTokenException
                {
                    int spaceIndex = text.IndexOfAny(new char[] { ' ', '\t', '\n', }, startIndex);
                    if (spaceIndex == -1) spaceIndex = text.Length - 1;
                    string lexem = text.Substring(startIndex, spaceIndex - startIndex + 1);
                    throw new InvalidTokenException(new Token(TokenType.Invalid, lexem, lineNumber));
                }
            }
            return result;
        }
    }
}