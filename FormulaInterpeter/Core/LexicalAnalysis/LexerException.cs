﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FormulaInterpreter.Core
{

    [Serializable]
    public abstract class LexerException : InterpreterException
    {
        public LexerException() { }
        public LexerException(string message) : base(message) { }
        public LexerException(string message, Exception inner) : base(message, inner) { }
        protected LexerException(
          System.Runtime.Serialization.SerializationInfo info,
          System.Runtime.Serialization.StreamingContext context) : base(info, context) { }
    }
    [Serializable]
    public class InvalidTokenException : LexerException
    {
        public Token Token { get; private set; }
        public InvalidTokenException(Token token) :
            this("Invalid token - \"" + token.Lexem + "\" line number - " + token.LineNumber, null, token)
        { }
        public InvalidTokenException(string message, Token token) :
            this(message, null, token)
        { }
        public InvalidTokenException(string message, Exception inner, Token token) :
            base(message, inner)
        {
            Token = token;
        }
        protected InvalidTokenException(
          System.Runtime.Serialization.SerializationInfo info,
          System.Runtime.Serialization.StreamingContext context) : base(info, context)
        {
            Token = null;
        }
    }
}
