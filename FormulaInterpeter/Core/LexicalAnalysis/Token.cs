﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FormulaInterpreter.Core
{
    public enum TokenType
    {
        Invalid,
        Keyword, Type, Identifier,
        Bracket, Operation, Spliter, Statement,
        StringLiteral, IntegerLiteral, RealLiteral, BooleanLiteral
    }
    public class Token
    {
        public int LineNumber { get; private set; }
        public string Lexem { get; private set; }

        public TokenType Type
        {
            get;
            private set;
        }
        public Token(TokenType type, string lexem, int lineNumber)
        {
            Type = type;
            Lexem = lexem;
            LineNumber = lineNumber;
        }
        public override bool Equals(object obj)
        {
            Token snd = obj as Token;
            if (snd == null) return false;
            if (this.Lexem != snd.Lexem) return false;
            if (this.LineNumber != snd.LineNumber) return false;
            if (this.Type != snd.Type) return false;
            return true;
        }
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
        public bool IsLiteral()
        {
            if (Type == TokenType.StringLiteral || Type == TokenType.RealLiteral ||
                Type == TokenType.IntegerLiteral || Type == TokenType.BooleanLiteral) return true;
            return false;
        }
    }
}