﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static FormulaInterpreter.Core.ParserGeneral;

namespace FormulaInterpreter.Core
{
    static class ExpressionParser
    {
        public static AST ToAST(List<Token> tokens, ref int startAt)
        {
            return Equalifier(tokens, ref startAt);
        }
        public static AST Identifier(List<Token> t, ref int s)
        {
            return ((t[s].Type == TokenType.Identifier) ? new AST(Rule.Variable, t[s++]) : null);
        }
        public static AST Literal(List<Token> t, ref int s)
        {
            return ((t[s].IsLiteral()) ? new AST(Rule.Literal, t[s++]) : null);
        }
        public static AST FunctionCall(List<Token> tokens, ref int startAt)
        {
            if (tokens[startAt].Type != TokenType.Identifier ||
                tokens[startAt + 1].Type != TokenType.Bracket ||
                tokens[startAt + 1].Lexem != "(") return null;
            Token token = tokens[startAt];
            startAt += 2;
            List<AST> nodes = SplittedNodes(tokens, ref startAt, ")", ToAST);
            return new AST(Rule.FunctionCall, token, nodes.ToArray());
        }
        private static AST Equalifier(List<Token> tokens, ref int startAt)
        {
            IsSuitableToken rule = (t) => t.Type == TokenType.Operation && (t.Lexem == "==" || t.Lexem == "!=");
            return BinaryOperationGeneral(tokens, ref startAt, rule, Comparier);
        }
        private static AST Comparier(List<Token> tokens, ref int startAt)
        {
            IsSuitableToken rule = (t) => t.Type == TokenType.Operation && (t.Lexem == ">=" || t.Lexem == "<=" || t.Lexem == ">" || t.Lexem == "<");
            return BinaryOperationGeneral(tokens, ref startAt, rule, Term);
        }
        private static AST Term(List<Token> tokens, ref int startAt)
        {
            IsSuitableToken rule = (t) => t.Type == TokenType.Operation && (t.Lexem == "+" || t.Lexem == "-");
            return BinaryOperationGeneral(tokens, ref startAt, rule, Factor);
        }
        private static AST Factor(List<Token> tokens, ref int startAt)
        {
            IsSuitableToken rule = (t) => t.Type == TokenType.Operation && (t.Lexem == "*" || t.Lexem == "/");
            return BinaryOperationGeneral(tokens, ref startAt, rule, UnaryOrExpression);
        }
        private static AST TypeCast(List<Token> tokens, ref int startAt)
        {
            if (tokens[startAt].Lexem != "(" || tokens[startAt + 1].Type != TokenType.Type
                || tokens[startAt + 2].Lexem != ")") return null;
            Token token = tokens[startAt + 1];
            startAt += 3;
            AST expression = UnaryOrExpression(tokens, ref startAt);
            if (expression == null) throw new ExpressionTreeException("You are missing expression");
            return new AST(Rule.TypeCast, token, expression);
        }
        private static AST UnaryOrExpression(List<Token> tokens, ref int startAt)
        {
            if (tokens[startAt].Type != TokenType.Operation ||
                !(tokens[startAt].Lexem == "+" || tokens[startAt].Lexem == "-"))
                return AnyOf(tokens, ref startAt, TypeCast, FunctionCall, Literal, Identifier, NextExpression);
            Token token = tokens[startAt++];
            AST expression = AnyOf(tokens, ref startAt, TypeCast, FunctionCall, Literal, Identifier, NextExpression);
            if (expression == null) throw new ExpressionTreeException("You are missing expression");
            return new AST(Rule.UnaryOp, token, expression);
        }
        // check for () and continue reqursion
        private static AST NextExpression(List<Token> tokens, ref int startAt)
        {
            if (tokens[startAt].Type == TokenType.Bracket && tokens[startAt].Lexem == "(")
            {
                startAt++;
                AST result = ToAST(tokens, ref startAt);
                if (tokens[startAt].Type == TokenType.Bracket && tokens[startAt].Lexem == ")")
                {
                    startAt++;
                    return result;
                }
                throw new BracketException("you are missing clossing bracket");
            }
            return null;
        }
        //general for arithmetical parser
        private static AST BinaryOperationGeneral(List<Token> tokens, ref int startAt, IsSuitableToken isSuitable, params GetASTNode[] nextNodes)
        {
            AST leftNode = AnyOf(tokens, ref startAt, nextNodes);
            if (leftNode == null) throw new ExpressionTreeException("expression doesn't follow any rule");
            while (isSuitable(tokens[startAt]))
            {
                Token token = tokens[startAt++];
                AST rightNode = AnyOf(tokens, ref startAt, nextNodes);
                if (rightNode == null) throw new ExpressionTreeException("expression doesn't follow any rule");
                leftNode = new AST(Rule.BinaryOp, token, leftNode, rightNode);
                if (startAt == tokens.Count) break;
            }
            return leftNode;
        }
    }
}
