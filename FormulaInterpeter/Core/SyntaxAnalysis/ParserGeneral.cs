﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FormulaInterpreter.Core
{
    delegate AST GetASTNode(List<Token> tokens, ref int startAt);
    delegate bool IsSuitableToken(Token token);
    static class ParserGeneral
    {
        //check node by node until not null
        public static AST AnyOf(List<Token> tokens, ref int startAt, params GetASTNode[] nodes)
        {
            for (int i = 0; i < nodes.Length; i++)
            {
                AST result = nodes[i](tokens, ref startAt);
                if (result != null) return result;
            }
            return null;
        }
        // reads  spilted nodes till ending lexem
        public static List<AST> SplittedNodes(List<Token> tokens, ref int startAt, string endingLexem, params GetASTNode[] rules)
        {
            List<AST> nodes = new List<AST>();
            bool start = true;
            while (tokens[startAt].Lexem != endingLexem)
            {
                if (start) start = false; // if(start)don't expect Spliter
                else
                {
                    if (tokens[startAt].Type != TokenType.Spliter) throw new GramarRuleException(", or" + endingLexem + " expected");
                    startAt++;
                }
                AST node = AnyOf(tokens, ref startAt, rules);
                if (node == null) throw new GramarRuleException("Invalid Rule"); //change to another exceptions and rethrow them in caller methods
                nodes.Add(node);
            }
            startAt++;
            return nodes;
        }
    }
}
