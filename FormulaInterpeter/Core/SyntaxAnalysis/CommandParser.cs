﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static FormulaInterpreter.Core.ParserGeneral;

namespace FormulaInterpreter.Core
{
    public static class Parser
    {
        public static List<AST> Analyse(List<Token> tokens)
        {
            try
            {
                int startAt = 0;
                var result = CommandParser.MultiCommandGeneral(tokens, ref startAt);
                if (startAt != tokens.Count) throw new BracketException("} doesn't have a pair");
                return result;
            }
            catch (ArgumentOutOfRangeException)
            {
                throw new MissingTokensException();
            }
        }
    }
    public static class CommandParser
    {
        static GetASTNode[] AvailableCommands = new GetASTNode[]
       {
                MultiCommand,
                Assignment,
                FunctionDeclaration,
                IdentifierDeclaration,
                IfCommands,
                In,
                Out,
                While
       };
        //should be last token or end with }
        public static List<AST> MultiCommandGeneral(List<Token> tokens, ref int startAt)
        {
            List<AST> result = new List<AST>();
            while (startAt != tokens.Count && tokens[startAt].Lexem != "}")
            {
                AST newNode = AnyOf(tokens, ref startAt, AvailableCommands);
                if (newNode == null) throw new GramarRuleException("Invalid grammar rules");
                result.Add(newNode);
            }
            return result;
        }
        //should be in {}
        private static AST MultiCommand(List<Token> tokens, ref int startAt)
        {
            if (tokens[startAt].Type != TokenType.Bracket || tokens[startAt].Lexem != "{")
                return null;
            var token = tokens[startAt++];
            List<AST> nodes = MultiCommandGeneral(tokens, ref startAt);
            if (tokens[startAt].Type != TokenType.Bracket || tokens[startAt].Lexem != "}")
                throw new BracketException("{ isn't closed");
            startAt++;
            return new AST(Rule.MultiCommand, token, nodes.ToArray());
        }
        private static AST IdentifierDeclaration(List<Token> tokens, ref int startAt)
        {
            if (tokens[startAt].Type != TokenType.Type) return null;
            Token type = tokens[startAt++];
            List<AST> nodes = SplittedNodes(tokens, ref startAt, ";", AssignmentGeneral, ExpressionParser.Identifier);
            if (nodes.Count == 0) throw new GramarRuleException("declaration can't be empty");
            return new AST(Rule.VariableDeclaration, type, nodes.ToArray());
        }
        private static AST ParameterDeclaration(List<Token> tokens, ref int startAt)
        {
            if (tokens[startAt].Type != TokenType.Type || tokens[startAt + 1].Type != TokenType.Identifier) return null;
            startAt += 2;
            return new AST(Rule.ParameterDeclaration, tokens[startAt - 2],
                new AST(Rule.Variable, tokens[startAt - 1]));
        }
        private static AST FunctionDeclaration(List<Token> tokens, ref int startAt)
        {
            if (tokens[startAt].Type != TokenType.Type ||
                tokens[startAt + 1].Type != TokenType.Identifier ||
                tokens[startAt + 2].Lexem != "(") return null;
            Token type = tokens[startAt++];
            Token identifier = tokens[startAt++];
            startAt++; // skip (
            List<AST> nodes = new List<AST>() {
                new AST(Rule.Variable,identifier),
            };
            nodes.AddRange(SplittedNodes(tokens, ref startAt, ")", ParameterDeclaration));
            AST realisation = MultiCommand(tokens, ref startAt);
            //TODO it's easy to split declaration and realisation
            if (realisation == null) throw new GramarRuleException("formula should be realised");
            nodes.Add(realisation);
            return new AST(Rule.FunctionDeclaration, type, nodes.ToArray());
        }
        private static AST In(List<Token> tokens, ref int startAt)
        {
            if (tokens[startAt].Type != TokenType.Keyword || tokens[startAt].Lexem != "in") return null;
            Token token = tokens[startAt++];
            List<AST> nodes = SplittedNodes(tokens, ref startAt, ";", ExpressionParser.Identifier);
            if (nodes.Count == 0) throw new GramarRuleException("in can't be empty");
            return new AST(Rule.In, token, nodes.ToArray());
        }
        private static AST Out(List<Token> tokens, ref int startAt)
        {
            if (tokens[startAt].Type != TokenType.Keyword || tokens[startAt].Lexem != "out") return null;
            Token token = tokens[startAt++];
            List<AST> nodes = SplittedNodes(tokens, ref startAt, ";", ExpressionParser.ToAST);
            if (nodes.Count == 0) throw new GramarRuleException("out can't be empty");
            return new AST(Rule.Out, token, nodes.ToArray());
        }
        private static AST AssignmentGeneral(List<Token> tokens, ref int startAt)
        {
            if (tokens[startAt].Type == TokenType.Identifier &&
                tokens[startAt + 1].Type == TokenType.Operation &&
                tokens[startAt + 1].Lexem == "=")
            {
                Token identifer = tokens[startAt];
                startAt += 2;
                AST value = ExpressionParser.ToAST(tokens, ref startAt);
                if (value == null) throw new GramarRuleException("wrong assignment format");
                return new AST(Rule.Assignment, identifer, value);
            }
            else return null;
        }
        //should end with;
        private static AST Assignment(List<Token> tokens, ref int startAt)
        {
            AST result = AssignmentGeneral(tokens, ref startAt);
            if (result == null) return null;
            if (tokens[startAt++].Type != TokenType.Statement) throw new GramarRuleException("you are missing statement token");
            return result;
        }
        private static AST SingleToMulti(AST command)
        {
            if (command.Rule == Rule.MultiCommand)
                return command;
            return new AST(Rule.MultiCommand, null, command);
        }
        private static AST IfCommands(List<Token> tokens, ref int startAt)
        {
            if (tokens[startAt].Type != TokenType.Keyword || tokens[startAt].Lexem != "if")
                return null;
            Token token = tokens[startAt++];
            if (tokens[startAt].Type != TokenType.Bracket || tokens[startAt].Lexem != "(")
                throw new BracketException("if should be followed by expression in ()");
            startAt++;
            AST condition = ExpressionParser.ToAST(tokens, ref startAt);
            if (tokens[startAt].Type != TokenType.Bracket || tokens[startAt].Lexem != ")")
                throw new BracketException("bracket ( isn't closed");
            startAt++;
            AST positive = SingleToMulti(AnyOf(tokens, ref startAt, AvailableCommands));
            if (positive == null) throw new GramarRuleException("Invalid Command after if");
            if (startAt == tokens.Count ||
                tokens[startAt].Type != TokenType.Keyword || tokens[startAt].Lexem != "else")
                return new AST(Rule.If, token, condition, positive);
            token = tokens[startAt++];
            AST negative = SingleToMulti(AnyOf(tokens, ref startAt, AvailableCommands));
            if (negative == null) throw new GramarRuleException("Invalid Command after else");
            return new AST(Rule.IfElse, token, condition, positive, negative);
        }
        private static AST While(List<Token> tokens, ref int startAt)
        {
            if (tokens[startAt].Type != TokenType.Keyword || tokens[startAt].Lexem != "while")
                return null;
            Token token = tokens[startAt++];
            if (tokens[startAt].Type != TokenType.Bracket || tokens[startAt].Lexem != "(")
                throw new BracketException("while should be followed by expression in ()");
            startAt++;
            AST condition = ExpressionParser.ToAST(tokens, ref startAt);
            if (tokens[startAt].Type != TokenType.Bracket || tokens[startAt].Lexem != ")")
                throw new BracketException("bracket ( isn't closed");
            startAt++;
            AST body = SingleToMulti(AnyOf(tokens, ref startAt, AvailableCommands));
            if (body == null) throw new Exception("Invalid Command after while");
            return new AST(Rule.While, token, condition, body);
        }
    }
}
