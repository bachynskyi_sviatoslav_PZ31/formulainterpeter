﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FormulaInterpreter.Core
{

    [Serializable]
    public class ParserException : InterpreterException
    {
        public ParserException() { }
        public ParserException(string message) : base(message) { }
        public ParserException(string message, Exception inner) : base(message, inner) { }
        protected ParserException(
          System.Runtime.Serialization.SerializationInfo info,
          System.Runtime.Serialization.StreamingContext context) : base(info, context) { }
    }
    [Serializable]
    public class BracketException : ParserException
    {
        public BracketException() { }
        public BracketException(string message) : base(message) { }
        public BracketException(string message, Exception inner) : base(message, inner) { }
        protected BracketException(
          System.Runtime.Serialization.SerializationInfo info,
          System.Runtime.Serialization.StreamingContext context) : base(info, context) { }
    }
    [Serializable]
    public class GramarRuleException : ParserException
    {
        public GramarRuleException() { }
        public GramarRuleException(string message) : base(message) { }
        public GramarRuleException(string message, Exception inner) : base(message, inner) { }
        protected GramarRuleException(
          System.Runtime.Serialization.SerializationInfo info,
          System.Runtime.Serialization.StreamingContext context) : base(info, context) { }
    }

    [Serializable]
    public class ExpressionTreeException : ParserException
    {
        public ExpressionTreeException() { }
        public ExpressionTreeException(string message) : base(message) { }
        public ExpressionTreeException(string message, Exception inner) : base(message, inner) { }
        protected ExpressionTreeException(
          System.Runtime.Serialization.SerializationInfo info,
          System.Runtime.Serialization.StreamingContext context) : base(info, context) { }
    }
    [Serializable]
    public class MissingTokensException : ParserException
    {
        public MissingTokensException() { }
        public MissingTokensException(string message) : base(message) { }
        public MissingTokensException(string message, Exception inner) : base(message, inner) { }
        protected MissingTokensException(
          System.Runtime.Serialization.SerializationInfo info,
          System.Runtime.Serialization.StreamingContext context) : base(info, context) { }
    }
}
