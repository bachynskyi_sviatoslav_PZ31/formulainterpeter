﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FormulaInterpreter.Core
{
    public enum Rule
    {
        VariableDeclaration, //+
        FunctionDeclaration, ParameterDeclaration, //+
        If, IfElse, //+
        MultiCommand, //+
        In, Out, //+
        Assignment, //+
        BinaryOp, //+
        UnaryOp, //+
        TypeCast, //+
        Variable, Literal, //+
        FunctionCall, //+
        While,
    }
    public class AST
    {
        //public Rule Rule { get; private set; }
        public Token Token { get; private set; }
        public List<AST> Nodes { get; private set; }
        public Rule Rule
        {
            get; private set;
        }
        public AST(Rule rule, Token token, params AST[] nodes)
        {
            Rule = rule;
            Token = token;
            if (nodes.Length == 0) Nodes = null;
            else
            {
                Nodes = new List<AST>(nodes);
            }
        }
    }
    
}
