﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FormulaInterpreter.Core
{
    interface ICommand
    {
        void Execute(MultiCommand scope);
    }
    class MultiCommand : ICommand
    {
        public Scope<string> Strings { get; private set; }
        public Scope<double> Reals { get; private set; }
        public Scope<int> Integers { get; private set; }
        public Scope<bool> Booleans { get; private set; }
        public Scope<FunctionDeclaration> Functions { get; private set; }
        public List<ICommand> Commands { get; private set; }
        public MultiCommand(List<ICommand> commands, List<string> strings, List<string> reals,
            List<string> integers, List<string> booleans, IDictionary<string, FunctionDeclaration> functions)
        {
            Commands = commands ?? new List<ICommand>();
            Strings = new Scope<string>(strings?.ToDictionary((s) => s, (s) => ""));
            Reals = new Scope<double>(reals?.ToDictionary((s) => s, (r) => 0.0));
            Integers = new Scope<int>(integers?.ToDictionary((s) => s, (i) => 0));
            Booleans = new Scope<bool>(booleans?.ToDictionary((s) => s, (b) => false));
            Functions = new Scope<FunctionDeclaration>(functions);
        }
        public MultiCommand(List<ICommand> commands, List<string> strings, List<string> reals,
            List<string> integers, List<string> booleans, List<string> functions)
            : this(commands, strings, reals, integers, booleans,
                  functions.ToDictionary((s) => s, (s) => (FunctionDeclaration)null))
        {
        }
        private MultiCommand(MultiCommand outerScope, MultiCommand copy)
        {
            Commands = copy.Commands;
            Integers = copy.Integers.MyCopy();
            Integers.OuterScope = outerScope?.Integers;
            Reals = copy.Reals.MyCopy();
            Reals.OuterScope = outerScope?.Reals;
            Booleans = copy.Booleans.MyCopy();
            Booleans.OuterScope = outerScope?.Booleans;
            Strings = copy.Strings.MyCopy();
            Strings.OuterScope = outerScope?.Strings;
            Functions = copy.Functions.MyCopy();
            Functions.OuterScope = outerScope?.Functions;
        }
        public void Execute(MultiCommand scope)
        {
            var clone = new MultiCommand(scope, this);
            foreach (var command in Commands)
                command.Execute(clone);
        }
    }
    class Assignment : ICommand
    {
        private VariableDeclaration Variable { get; set; }
        public IExpression Expression { get; set; }
        public Assignment(VariableDeclaration variable, IExpression expression)
        {
            this.Variable = variable;
            this.Expression = expression;
        }
        public void Execute(MultiCommand scope)
        {
            var ExpressionValue = Expression.Calculate(scope);
            Variable.Set(scope, ExpressionValue);
        }
    }
    class IfElse : ICommand
    {
        IExpression condition { get; set; }
        MultiCommand positive { get; set; }
        MultiCommand negative { get; set; }
        public IfElse(IExpression condition, MultiCommand positive, MultiCommand negative = null)
        {
            this.condition = condition;
            this.positive = positive;
            this.negative = negative;
        }
        public void Execute(MultiCommand scope)
        {
            if ((bool)condition.Calculate(scope)) positive.Execute(scope);
            else
                negative?.Execute(scope);
        }
    }
    class While : ICommand
    {
        IExpression condition { get; set; }
        MultiCommand body { get; set; }
        public While(IExpression condition, MultiCommand body)
        {
            this.condition = condition;
            this.body = body;
        }
        public void Execute(MultiCommand scope)
        {
            while ((bool)condition.Calculate(scope)) body.Execute(scope);
        }
    }
    class Out : ICommand
    {
        Code code { get; set; }
        List<IExpression> values { get; set; }
        public Out(Code code, List<IExpression> values)
        {
            this.code = code;
            this.values = values;
        }
        public void Execute(MultiCommand scope)
        {
            WriteArgs writeArgs = new WriteArgs();
            foreach (var value in values)
            {
                var str = (string)value.Calculate(scope);
                foreach (var ch in str)
                {
                    writeArgs.Char = ch;
                    code.write(code, writeArgs);
                }
            }
            writeArgs.Char = '\n';
            code.write(code, writeArgs);
        }
    }
    class In : ICommand
    {
        Code code { get; set; }
        List<VariableDeclaration> variables { get; set; }
        public In(Code code, List<VariableDeclaration> variables)
        {
            this.code = code; this.variables = variables;
        }
        delegate object ReadVariable(string input);
        static private object ReadInteger(string input)
        {
            return int.Parse(input);
        }
        static private object ReadReal(string input)
        {
            return double.Parse(input);
        }
        static private object ReadBoolean(string input)
        {
            return bool.Parse(input);
        }
        static private object ReadString(string input)
        {
            return input;
        }
        static Dictionary<ReturnType, ReadVariable> readers = new Dictionary<ReturnType, ReadVariable>
            {
                {ReturnType.Integer, ReadInteger },
            {ReturnType.Real, ReadReal },
            {ReturnType.Boolean, ReadBoolean },
            {ReturnType.String, ReadString },
            };
        public void Execute(MultiCommand scope)
        {
            var readArgs = new ReadArgs();
                object result;
            foreach (var variable in variables)
            {
                var read = readers[variable.Type];
                while (true)
                    try
                    {
                        StringBuilder input = new StringBuilder();
                        readArgs.Char = ' ';
                        while(Char.IsWhiteSpace(readArgs.Char))
                        {
                            code.read(code, readArgs);
                        }
                        while (!Char.IsWhiteSpace(readArgs.Char))
                        {
                            input.Append(readArgs.Char);
                            code.read(code, readArgs);
                        }
                        result = read(input.ToString());
                        break;
                    }
                    catch (FormatException)
                    {
                    }
                variable.Set(scope, result);
            }
        }
    }
}
