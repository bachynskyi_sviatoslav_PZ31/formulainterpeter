﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FormulaInterpreter.Core
{
    interface IExpression
    {
        object Calculate(MultiCommand scope);
    }
    class DefaultValue<T> : IExpression
    {
        public object Calculate(MultiCommand scope)
        {
            return default(T);
        }
    }
    static class DefaultValue
    {
        static IExpression Integer = new DefaultValue<int>();
        static IExpression Real = new DefaultValue<double>();
        static IExpression Boolean = new DefaultValue<bool>();
        static IExpression String = new DefaultValue<string>();
        public static IExpression Of(ReturnType type)
        {
            switch (type)
            {
                case ReturnType.Boolean: return Boolean;
                case ReturnType.Integer: return Integer;
                case ReturnType.Real: return Real;
                case ReturnType.String: return String;
                default: throw new TypeException();
            }
        }
    }
    class Variable : IExpression
    {
        VariableDeclaration variableDeclaration { get; set; }
        public Variable(VariableDeclaration variableDeclaration)
        {
            this.variableDeclaration = variableDeclaration;
        }
        public object Calculate(MultiCommand scope)
        {
            return variableDeclaration.Get(scope);
        }
    }
    class Literal: IExpression
    {
        object value;
        public Literal(string value, ReturnType type)
        {
            switch (type)
            {
                case ReturnType.Boolean:
                    this.value = (value) == "true";
                    break;
                case ReturnType.Integer:
                    this.value = Int32.Parse(value);
                    break;
                case ReturnType.Real:
                    this.value = Double.Parse(value);
                    break;
                case ReturnType.String:
                    this.value = value.Substring(1,value.Length-2);
                    break;
                default:
                    throw new TypeException();
            }
        }
        public object Calculate(MultiCommand scope)
        {
            return value;
        }
    }
    class FunctionCall : IExpression
    {
        string identifier { get; set; }
        List<IExpression> parameters { get; set; }
        public FunctionCall(string identifier, List<IExpression> parameters)
        {
            this.identifier = identifier;
            this.parameters = parameters;
        }
        public object Calculate(MultiCommand scope)
        {
            return scope.Functions.Get(identifier).Calculate(scope,parameters);
        }
    }
    class TypeCast : IExpression
    {
        IExpression expression { get; set; }
        ReturnType wantedType { get; set; }
        public TypeCast(IExpression expression,ReturnType wantedType)
        {
            this.expression = expression;
            this.wantedType = wantedType;
        }
        public object Calculate(MultiCommand scope)
        {
            object value = expression.Calculate(scope);
            if (wantedType == ReturnType.String) return value.ToString();
            if(wantedType == ReturnType.Integer && value is double) return (int)(double)value;
            if (wantedType == ReturnType.Real && value is int) return (double)(int)value;
            throw new TypeException();
        }
    }
    delegate object CalculateUnary(MultiCommand scope,ReturnType type, IExpression expression);
    class UnaryOpeartion : IExpression
    {
        static Dictionary<string, CalculateUnary> Operations =
            new Dictionary<string, CalculateUnary>()
            {
                { "+", Plus},
                { "-", Minus },
            };
        static object Plus(MultiCommand scope,ReturnType type, IExpression expression)
        {
            return expression.Calculate(scope);
        }
        static object Minus(MultiCommand scope,ReturnType type, IExpression expression)
        {
            if(type == ReturnType.Integer)
            return -(int)expression.Calculate(scope);
            if (type == ReturnType.Real)
                return -(double)expression.Calculate(scope);
            throw new TypeException();
        }
        ReturnType type { get; set; }
        IExpression expression { get; set; }
        CalculateUnary calculation { get; set; }
        public UnaryOpeartion(string operation, ReturnType type, IExpression expression)
        {
            this.type = type;
            this.expression = expression;
            calculation = Operations[operation];
        }
        public object Calculate(MultiCommand scope)
        {
            return calculation(scope,type,expression);
        }
    }
    delegate object CalculateBinary(MultiCommand scope,IExpression left, IExpression right);
    class BinaryOperationInteger : IExpression
    {
        static Dictionary<string, CalculateBinary> operations = new Dictionary<string, CalculateBinary>()
        {
            {"+", Plus },
            { "-", Minus },
            { "/", Division },
            {"*", Multiplictaion },
            { "==", Equality },
            { "!=", InEqualtiy},
            { ">=", Greater},
            { "<=", Less},
            { ">", StrictGreater},
            { "<", StrictLess },
        };
        static object Plus(MultiCommand scope,IExpression left, IExpression right)
        {
            return (int)left.Calculate(scope) + (int)right.Calculate(scope);
        }
        static object Minus(MultiCommand scope, IExpression left, IExpression right)
        {
            return (int)left.Calculate(scope) - (int)right.Calculate(scope);
        }
        static object Division(MultiCommand scope,IExpression left, IExpression right)
        {
            return (int)left.Calculate(scope) / (int)right.Calculate(scope);
        }
        static object Multiplictaion(MultiCommand scope, IExpression left, IExpression right)
        {
            return (int)left.Calculate(scope) * (int)right.Calculate(scope);
        }
        static object Equality(MultiCommand scope, IExpression left, IExpression right)
        {
            return (int)left.Calculate(scope) == (int)right.Calculate(scope);
        }
        static object InEqualtiy(MultiCommand scope, IExpression left, IExpression right)
        {
            return (int)left.Calculate(scope) != (int)right.Calculate(scope);
        }
        static object StrictGreater(MultiCommand scope, IExpression left, IExpression right)
        {
            return (int)left.Calculate(scope) > (int)right.Calculate(scope);
        }
        static object StrictLess(MultiCommand scope, IExpression left, IExpression right)
        {
            return (int)left.Calculate(scope) < (int)right.Calculate(scope);
        }
        static object Less(MultiCommand scope, IExpression left, IExpression right)
        {
            return (bool)StrictLess(scope, left,right) || (bool)Equality(scope, left,right);
        }
        static object Greater(MultiCommand scope, IExpression left, IExpression right)
        {
            return (bool)StrictGreater(scope, left, right) || (bool)Equality(scope, left, right);
        }
        IExpression left { get; set; }
        IExpression right { get; set; }
        CalculateBinary calculation;
        public BinaryOperationInteger(string operation, IExpression left, IExpression right)
        {
            this.left = left;
            this.right = right;
            calculation = operations[operation];
        }
        public object Calculate(MultiCommand scope)
        {
            return calculation(scope,left, right);
        }
    }
    class BinaryOperationReal : IExpression
    {
        static Dictionary<string, CalculateBinary> operations = new Dictionary<string, CalculateBinary>()
        {
            {"+", Plus },
            { "-", Minus },
            { "/", Division },
            {"*", Multiplictaion },
            { "==", Equality },
            { "!=", InEqualtiy},
            { ">=", Greater},
            { "<=", Less},
            { ">", StrictGreater},
            { "<", StrictLess },
        };
        static object Plus(MultiCommand scope, IExpression left, IExpression right)
        {
            return (double)left.Calculate(scope) + (double)right.Calculate(scope);
        }
        static object Minus(MultiCommand scope, IExpression left, IExpression right)
        {
            return (double)left.Calculate(scope) - (double)right.Calculate(scope);
        }
        static object Division(MultiCommand scope, IExpression left, IExpression right)
        {
            return (double)left.Calculate(scope) / (double)right.Calculate(scope);
        }
        static object Multiplictaion(MultiCommand scope, IExpression left, IExpression right)
        {
            return (double)left.Calculate(scope) * (double)right.Calculate(scope);
        }
        static object Equality(MultiCommand scope, IExpression left, IExpression right)
        {
            return (double)left.Calculate(scope) == (double)right.Calculate(scope);
        }
        static object InEqualtiy(MultiCommand scope, IExpression left, IExpression right)
        {
            return (double)left.Calculate(scope) != (double)right.Calculate(scope);
        }
        static object StrictGreater(MultiCommand scope, IExpression left, IExpression right)
        {
            return (double)left.Calculate(scope) > (double)right.Calculate(scope);
        }
        static object StrictLess(MultiCommand scope, IExpression left, IExpression right)
        {
            return (double)left.Calculate(scope) < (double)right.Calculate(scope);
        }
        static object Less(MultiCommand scope, IExpression left, IExpression right)
        {
            return (bool)StrictLess(scope, left, right) || (bool)Equality(scope, left, right);
        }
        static object Greater(MultiCommand scope, IExpression left, IExpression right)
        {
            return (bool)StrictGreater(scope, left, right) || (bool)Equality(scope, left, right);
        }
        IExpression left { get; set; }
        IExpression right { get; set; }
        CalculateBinary calculation;
        public BinaryOperationReal(string operation,IExpression left, IExpression right)
        {
            this.left = left;
            this.right = right;
            calculation = operations[operation];
        }
        public object Calculate(MultiCommand scope)
        {
            return calculation(scope, left, right);
        }
    }
    class BinaryOperationBoolean : IExpression
    {
        static Dictionary<string, CalculateBinary> operations = new Dictionary<string, CalculateBinary>()
        {
            { "==", Equality },
            { "!=", InEqualtiy},
        };
        
        static object Equality(MultiCommand scope, IExpression left, IExpression right)
        {
            return (bool)left.Calculate(scope) == (bool)right.Calculate(scope);
        }
        static object InEqualtiy(MultiCommand scope, IExpression left, IExpression right)
        {
            return (bool) left.Calculate(scope) != (bool)right.Calculate(scope);
        }
        IExpression left { get; set; }
        IExpression right { get; set; }
        CalculateBinary calculation;
        public BinaryOperationBoolean(string operation,IExpression left, IExpression right)
        {
            this.left = left;
            this.right = right;
            calculation = operations[operation];
        }
        public object Calculate(MultiCommand scope)
        {
            return calculation(scope, left, right);
        }
    }
    class BinaryOperationString : IExpression
    {
        static Dictionary<string, CalculateBinary> operations = new Dictionary<string, CalculateBinary>()
        {
            {"+", Plus },
            { "==", Equality },
            { "!=", InEqualtiy},
        };
        static object Plus(MultiCommand scope, IExpression left, IExpression right)
        {
            return (string)left.Calculate(scope) + (string)right.Calculate(scope);
        }
        static object Equality(MultiCommand scope, IExpression left, IExpression right)
        {
            return (string)left.Calculate(scope) == (string)right.Calculate(scope);
        }
        static object InEqualtiy(MultiCommand scope, IExpression left, IExpression right)
        {
            return (string)left.Calculate(scope) != (string)right.Calculate(scope);
        }
        IExpression left { get; set; }
        IExpression right { get; set; }
        CalculateBinary calculation;
        public BinaryOperationString(string operation,IExpression left, IExpression right)
        {
            this.left = left;
            this.right = right;
            calculation = operations[operation];
        }
        public object Calculate(MultiCommand scope)
        {
            return calculation(scope, left, right);
        }
    }
}
