﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FormulaInterpreter.Core
{
    public class WriteArgs : EventArgs
    {
        public char Char
        {
            get; internal set;
        }
    }
    public class ReadArgs : EventArgs
    {
        public char Char
        {
            internal get; set;
        }
    }
    public class Code
    {
        //assure signle event
        internal MultiCommand multiCommand
        {
            private get;
            set;
        }
        static void EventAdd<TArgs>(ref EventHandler<TArgs> deleg, EventHandler<TArgs> value)
        {
            if (deleg != null) throw new Exception("you must remove old delegate first");
            deleg = value;
        }
        void EventRemove<TArgs>(ref EventHandler<TArgs> deleg, EventHandler<TArgs> value)
        {
            if (deleg != null && deleg != value) throw new Exception("deleting of wrong delegate");
            deleg = null;
        }
        internal EventHandler<WriteArgs> write;
        public event EventHandler<WriteArgs> onWrite
        {
            add
            {
                EventAdd(ref write, value);
            }
            remove
            {
                EventRemove(ref write, value);
            }
        }
        internal EventHandler<ReadArgs> read;
        public event EventHandler<ReadArgs> onRead
        {
            add
            {
                EventAdd(ref read, value);
            }
            remove
            {
                EventRemove(ref read, value);
            }
        }
        internal Code()
        {
            this.multiCommand = null;
        }
        public void Execute()
        {
            multiCommand.Execute(null);
        }

    }
}
