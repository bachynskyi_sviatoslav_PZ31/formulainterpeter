﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FormulaInterpreter.Core
{
    class VariableDeclaration
    {
        public string Name { get; private set; }
        public ReturnType Type { get; private set; }
        public VariableDeclaration(string name, ReturnType type)
        {
            Name = name;
            Type = type;
        }
        public void Set(MultiCommand scope, object value)
        {
            switch (this.Type)
            {
                case ReturnType.String:
                    scope.Strings.Set(Name, (string)value);
                    break;
                case ReturnType.Real:
                    scope.Reals.Set(Name, (double)value);
                    break;
                case ReturnType.Integer:
                    scope.Integers.Set(Name, (int)value);
                    break;
                case ReturnType.Boolean:
                    scope.Booleans.Set(Name, (bool)value);
                    break;
                default:
                    throw new TypeException();
            }
        }
        public object Get(MultiCommand scope)
        {
            switch (Type)
            {
                case ReturnType.String:
                    return scope.Strings.Get(Name);
                case ReturnType.Real:
                    return scope.Reals.Get(Name);
                case ReturnType.Integer:
                    return scope.Integers.Get(Name);
                case ReturnType.Boolean:
                    return scope.Booleans.Get(Name);
                default:
                    throw new TypeException();
            }
        }
    }
}
