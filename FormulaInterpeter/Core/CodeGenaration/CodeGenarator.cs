﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FormulaInterpreter.Core
{
    static public class CodeGenarator
    {
        public static Code Run(Scope scope)
        {
            var code = new Code();
            var multiCommand = CommandGenerator.MultiCommand(code, scope);
            code.multiCommand = multiCommand;
            return code;
        }
    }

    static class CommandGenerator
    {
        static List<string> GetVariableOfType(this Scope scope, ReturnType type)
        {
            return scope.Variables.InnerDeclarations.Where((kv) => kv.Value.Type == type).
                 Select((kv) => kv.Key).ToList();
        }
        public static MultiCommand MultiCommand(Code code, Scope scope)
        {
            List<string> strings = scope.GetVariableOfType(ReturnType.String),
                reals = scope.GetVariableOfType(ReturnType.Real),
                integers = scope.GetVariableOfType(ReturnType.Integer),
                booleans = scope.GetVariableOfType(ReturnType.Boolean);
            List<string> functions = scope.Functions.InnerDeclarations.Select((kv) => kv.Key).ToList();
            MultiCommand res = new MultiCommand(null, strings, reals, integers, booleans, functions);
            foreach (var functionScope in scope.Functions.InnerDeclarations)
            {
                FunctionDeclaration(code, res, functionScope.Key, functionScope.Value);
            }
            foreach (var command in scope.Nodes)
            {
                ICommand createdCommand;
                switch (command.Rule)
                {
                    case Rule.Assignment:
                        createdCommand = Assignment(command);
                        break;
                    case Rule.If:
                    case Rule.IfElse:
                        createdCommand = IfElse(code, command);
                        break;
                    case Rule.While:
                        createdCommand = While(code, command);
                        break;
                    case Rule.MultiCommand:
                        createdCommand = MultiCommand(code, (Scope)command);
                        break;
                    case Rule.Out:
                        createdCommand = Out(code, command);
                        break;
                    case Rule.In:
                        createdCommand = In(code, command);
                        break;
                    default: throw new UnsupportedCommandException();
                }
                res.Commands.Add(createdCommand);
            }
            return res;
            throw new NotImplementedException();
        }
        static private ICommand Out(Code code, DST command)
        {
            return new Out(code, command.Nodes.Select((n) => ExpressionGenerator.Run(n)).ToList());
        }
        static private ICommand In(Code code, DST command)
        {
            return new In(code, command.Nodes.Select((c) =>
            new VariableDeclaration(c.Token.Lexem, c.ReturnType)).ToList());
        }
        static private ICommand Assignment(DST command)
        {
            var variableDeclaration = new VariableDeclaration(
                command.Token.Lexem,
                command.Nodes[0].ReturnType
                );
            return new Assignment(variableDeclaration, ExpressionGenerator.Run(command.Nodes[0]));
        }
        static private ICommand IfElse(Code code, DST command)
        {
            var condition = ExpressionGenerator.Run(command.Nodes[0]);
            var positive = MultiCommand(code, (Scope)command.Nodes[1]);
            var negative = (MultiCommand)null;
            if (command.Rule == Rule.IfElse)
            {
                negative = MultiCommand(code, (Scope)command.Nodes[2]);
            }
            return new IfElse(condition, positive, negative);
        }
        static private ICommand While(Code code, DST command)
        {
            var conidition = ExpressionGenerator.Run(command.Nodes[0]);
            var body = MultiCommand(code, (Scope)command.Nodes[1]);
            return new While(conidition, body);
        }
        private static void FunctionDeclaration(Code code, MultiCommand scope, string identifier, FunctionScope functionScope)
        {
            List<VariableDeclaration> parameters = functionScope.Parameteres.Select(
                (kv) => new VariableDeclaration(kv.Key, kv.Value.Type)).ToList();
            scope.Functions.Set(identifier, new FunctionDeclaration(functionScope.Result.Type, parameters,
                 MultiCommand(code, functionScope)));
        }
    }
    static class ExpressionGenerator
    {
        delegate IExpression ExpressionAnalyzer(DST command);
        public static IExpression Run(DST command)
        {
            Dictionary<Rule, ExpressionAnalyzer> expressions =
                new Dictionary<Rule, ExpressionAnalyzer>()
                {
                    {Rule.Literal,Literal },
                    {Rule.Variable,Variable },
                    {Rule.FunctionCall, FunctionCall },
                    {Rule.UnaryOp, UnaryOp},
                    {Rule.BinaryOp,BinaryOp },
                    {Rule.TypeCast,TypeCast },
                };
            return expressions[command.Rule](command);
        }
        private static IExpression Literal(DST command)
        {
            return new Literal(command.Token.Lexem, command.ReturnType);
        }
        private static IExpression Variable(DST command)
        {
            return new Variable(new VariableDeclaration(command.Token.Lexem, command.ReturnType));
        }
        private static IExpression FunctionCall(DST command)
        {
            return new FunctionCall(command.Token.Lexem,
                command.Nodes.Select((dst) => Run(dst)).ToList());
        }
        private static IExpression UnaryOp(DST command)
        {
            return new UnaryOpeartion(command.Token.Lexem, command.ReturnType,
                Run(command.Nodes[0]));
        }
        private static IExpression BinaryOp(DST command)
        {
            IExpression left = Run(command.Nodes[0]);
            IExpression right = Run(command.Nodes[1]);
            switch (command.Nodes[0].ReturnType)
            {
                case ReturnType.Integer:
                    return new BinaryOperationInteger(command.Token.Lexem, left, right);
                case ReturnType.Boolean:
                    return new BinaryOperationBoolean(command.Token.Lexem, left, right);
                case ReturnType.Real:
                    return new BinaryOperationReal(command.Token.Lexem, left, right);
                case ReturnType.String:
                    return new BinaryOperationString(command.Token.Lexem, left, right);

                default: throw new TypeException();
            }
        }
        private static IExpression TypeCast(DST command)
        {
            IExpression expression = Run(command.Nodes[0]);
            return new TypeCast(expression, command.ReturnType);
        }
    }
}
