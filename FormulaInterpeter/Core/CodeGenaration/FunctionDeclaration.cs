﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FormulaInterpreter.Core
{
    
    class FunctionDeclaration
    {
        MultiCommand functionScopeSample { get; set; }
        List<VariableDeclaration> parameters { get; set; }
        ReturnType resultType { get; set; }
        public FunctionDeclaration(ReturnType resultType, List<VariableDeclaration> parameters,MultiCommand functionScope)
        {
            this.resultType = resultType;
            this.parameters = parameters;
            this.functionScopeSample = functionScope;
        }
        public object Calculate(MultiCommand scope, List<IExpression> parametersValues)
        {
            //copy functionScope to allow recursion
            var functionScope = new MultiCommand(functionScopeSample.Commands,
                functionScopeSample.Strings.InnerDeclarations.Keys.ToList(),
                functionScopeSample.Reals.InnerDeclarations.Keys.ToList(),
                functionScopeSample.Integers.InnerDeclarations.Keys.ToList(),
                functionScopeSample.Booleans.InnerDeclarations.Keys.ToList(),
                scope.Functions.AllDeclarations);

            for(int i=0;i<parameters.Count;i++)
            {
                var parameter = parameters[i];
                parameter.Set(functionScope, parametersValues[i].Calculate(scope));
            }
            var result = new VariableDeclaration("result", resultType);
            /*new Assignment(result,
                DefaultValue.Of(resultType)).Execute(functionScope);*/
            functionScope.Execute(null);
            return result.Get(functionScope);
        }
    }
}
