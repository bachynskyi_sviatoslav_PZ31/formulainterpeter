﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FormulaInterpreter.Core
{
    
    public class Scope<T>
    {
        private Dictionary<string, T> declarations = new Dictionary<string, T>();
        public IDictionary<string, T> InnerDeclarations
        {
            get => declarations;
        }
        public IDictionary<string, T> AllDeclarations
        {
            get
            {
                Dictionary<string, T> result = new Dictionary<string, T>(declarations);
                if (OuterScope != null)
                    foreach (var kv in OuterScope.AllDeclarations)
                        result.Add(kv.Key, kv.Value);
                return result;
            }
        }
        public Scope<T> OuterScope { get; set; }
        public Scope(IDictionary<string, T> declarations, Scope<T> outerScope = null)
        {
            this.OuterScope = outerScope;
            foreach (var keyValue in declarations)
            {
                Add(keyValue.Key, keyValue.Value);
            }
        }
        public Scope<T> MyCopy()
        {
            return (Scope<T>)this.MemberwiseClone();
        }
        public bool Contains(string name)
        {
            if (OuterScope != null && OuterScope.Contains(name)) return true;
            return declarations.ContainsKey(name);
        }
        public T Get(string name)
        {
            if (OuterScope != null && OuterScope.Contains(name)) return OuterScope.Get(name);
            if (!declarations.ContainsKey(name))
                throw new UndeclaredIdentifierException("there is no " + name);
            return declarations[name];
        }
        public void Set(string name, T value)
        {
            if (OuterScope != null && OuterScope.Contains(name))
            {
                OuterScope.Set(name, value);
                return;
            }
            if (!declarations.ContainsKey(name))
                throw new UndeclaredIdentifierException("there is no " + name);
            declarations[name] = value;
        }
        public void Add(string name, T declaration)
        {
            if (Contains(name)) throw new MultipleDeclarationException(name);
            declarations.Add(name, declaration);
        }
    }
}
