﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FormulaInterpreter.Core
{
    public class VariableStatus
    {
        public bool Initialised { get; set; }
        public ReturnType Type
        {
            get; private set;
        }
        public VariableStatus(ReturnType type, bool initialised = false)
        {
            Type = type; Initialised = initialised;
        }
    }
    public class Scope : DST
    {
        //public Scope<FunctionScope> Functions { get; protected set; }
        //public Scope<VariableStatus> Variables { get; protected set; }
        public Scope<FunctionScope> Functions
        {
            get;
            protected set;
        }
        public Scope<VariableStatus> Variables
        {
            get;
            protected set;
        }
        public Scope(Scope outerScope = null, List<DST> commands = null,
            IDictionary<string, VariableStatus> variables = null,
                    IDictionary<string, FunctionScope> functions = null) :
            base(Rule.MultiCommand, ReturnType.Void, null, commands?.ToArray() ?? new DST[0])
        {
            this.Variables = new Scope<VariableStatus>(
                variables ?? new Dictionary<string, VariableStatus>(),
                outerScope?.Variables);
            this.Functions = new Scope<FunctionScope>(
                functions ?? new Dictionary<string, FunctionScope>(),
                outerScope?.Functions);
        }
    }
    public class FunctionScope : Scope
    {
        public Dictionary<string, VariableStatus> Parameteres { get; private set; }
        public VariableStatus Result { get; private set; }
        public FunctionScope(ReturnType returnType = ReturnType.Void, Scope outerScope = null,
            List<DST> commands = null, Dictionary<string, VariableStatus> variables = null,
                    IDictionary<string, FunctionScope> functions = null,
                    IDictionary<string, VariableStatus> parameteres = null)
            : base(outerScope, commands, variables, functions)
        {
            Result = new VariableStatus(returnType, false);
            ReturnType = returnType;
            if (parameteres != null)
                Parameteres = new Dictionary<string, VariableStatus>(parameteres);
            else Parameteres = new Dictionary<string, VariableStatus>();
            this.Variables = new Scope<VariableStatus>(Parameteres);
            Variables.Add("result", Result);
            if (variables != null)
                foreach (var variable in variables) this.Variables.Add(variable.Key, variable.Value);
        }
    }
}
