﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FormulaInterpreter.Core
{
    static class ExpressionAnalyzer
    {
        private static DST AddConversion(DST value, ReturnType wantedType)
        {
            return new DST(Rule.TypeCast, wantedType,
                new Token(TokenType.Type, ReturnTypeConverter.ToString(wantedType), -1), value);
        }
        private static DST ImplicitConversion(DST value, ReturnType wantedType)
        {
            if (value.ReturnType == wantedType) return value;
            if (wantedType == ReturnType.String && value.ReturnType != ReturnType.Void)
            {
                return AddConversion(value, wantedType);
            }
            if (wantedType == ReturnType.Real && value.ReturnType == ReturnType.Integer)
                return AddConversion(value, wantedType);
            throw new ImplicitConversionException();
        }
        public static DST OfType(Scope scope, AST expression, ReturnType wantedType)
        {
            return ImplicitConversion(AnalyseExpression(scope, expression), wantedType);
        }
        private static DST AnalyseExpression(Scope scope, AST expression)
        {
            switch (expression.Rule)
            {
                case Rule.Literal: return Literal(scope, expression);
                case Rule.Variable: return Variable(scope, expression);
                case Rule.FunctionCall: return FunctionCall(scope, expression);
                case Rule.TypeCast: return TypeCast(scope, expression);
                case Rule.UnaryOp: return UnaryOp(scope, expression);
                case Rule.BinaryOp: return BinaryOp(scope, expression);
            }
            throw new UnsupportedOperationException();
        }
        private static DST FunctionCall(Scope scope, AST call)
        {
            var function = scope.Functions.Get(call.Token.Lexem);
            if (call.Nodes.Count != function.Parameteres.Count)
                throw new FunctionException("parameter number doesn't match");
            var paramEnum = function.Parameteres.GetEnumerator();
            var callEnum = call.Nodes.GetEnumerator();
            var paramValues = new List<DST>();
            while (paramEnum.MoveNext())
            {
                callEnum.MoveNext();
                paramValues.Add(OfType(scope, callEnum.Current, paramEnum.Current.Value.Type));
            }
            return new DST(Rule.FunctionCall, function.ReturnType, call.Token, paramValues.ToArray());
        }
        private static DST Literal(Scope scope, AST expression)
        {
            return new DST(expression.Rule, ReturnTypeConverter.FromLiteral(expression), expression.Token);
        }
        private static DST Variable(Scope scope, AST expression)
        {
            var variable = scope.Variables.Get(expression.Token.Lexem);
            if (variable == null) throw new VariableException("variable cannot be used before declaration");
            if (!variable.Initialised) throw new VariableException("Variable isn't initialised");
            return new DST(expression.Rule, scope.Variables.Get(expression.Token.Lexem).Type, expression.Token);
        }
        private static DST ExplicitConversion(DST expression, ReturnType wantedType)
        {
            if (expression.ReturnType == ReturnType.Real && wantedType == ReturnType.Integer)
            {
                return AddConversion(expression, wantedType);
            }
            return ImplicitConversion(expression, wantedType);
        }
        private static DST TypeCast(Scope scope, AST expression)
        {
            return ExplicitConversion(AnalyseExpression(scope, expression.Nodes[0]),
                ReturnTypeConverter.FromString(expression.Token.Lexem));
        }
        private static DST UnaryOp(Scope scope, AST expression)
        {
            DST innerExpression = AnalyseExpression(scope, expression.Nodes[0]);
            if (innerExpression.ReturnType != ReturnType.Integer && innerExpression.ReturnType != ReturnType.Real)
                throw new UnsupportedOperationException();
            return new DST(Rule.UnaryOp, innerExpression.ReturnType, expression.Token, innerExpression);
        }
        private static DST BinaryOp(Scope scope, AST expression)
        {
            DST leftNode = AnalyseExpression(scope, expression.Nodes[0]);
            DST rightNode = AnalyseExpression(scope, expression.Nodes[1]);
            if (leftNode.ReturnType == ReturnType.String || rightNode.ReturnType == ReturnType.String)
            {
                if (expression.Token.Lexem == "+") return new DST(Rule.BinaryOp, ReturnType.String, expression.Token,
                     ImplicitConversion(leftNode, ReturnType.String), ImplicitConversion(rightNode, ReturnType.String));
                if (OperationType.IsEqualifier(expression.Token)
                    && leftNode.ReturnType == ReturnType.String &&
                    rightNode.ReturnType == ReturnType.String) return new DST(Rule.BinaryOp, ReturnType.Boolean, expression.Token,
                        leftNode, rightNode);
                throw new UnsupportedOperationException();
            }
            if (leftNode.ReturnType == ReturnType.Boolean || rightNode.ReturnType == ReturnType.Boolean)
            {
                if (leftNode.ReturnType == ReturnType.Boolean && rightNode.ReturnType == ReturnType.Boolean
                    && OperationType.IsEqualifier(expression.Token))
                    return new DST(Rule.BinaryOp, ReturnType.Boolean, expression.Token, leftNode, rightNode);
                throw new UnsupportedOperationException();
            }
            if (leftNode.ReturnType == ReturnType.Real || rightNode.ReturnType == ReturnType.Real)
            {
                ReturnType resultType = ReturnType.Real;
                if (OperationType.IsRelational(expression.Token))
                {
                    resultType = ReturnType.Boolean;
                }
                return new DST(Rule.BinaryOp, resultType, expression.Token,
                   ImplicitConversion(leftNode, ReturnType.Real), ImplicitConversion(rightNode, ReturnType.Real));
            }
            if (leftNode.ReturnType == ReturnType.Integer && rightNode.ReturnType == ReturnType.Integer)
            {
                ReturnType resultType = ReturnType.Integer;
                if (OperationType.IsRelational(expression.Token))
                {
                    resultType = ReturnType.Boolean;
                }
                return new DST(Rule.BinaryOp, resultType, expression.Token, leftNode, rightNode);
            }
            throw new UnsupportedOperationException();
        }
    }
}
