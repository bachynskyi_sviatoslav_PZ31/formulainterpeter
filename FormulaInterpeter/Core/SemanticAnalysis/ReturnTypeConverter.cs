﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FormulaInterpreter.Core
{
    static class ReturnTypeConverter
    {
        public static ReturnType FromString(string lexem)
        {
            switch (lexem)
            {
                case "integer": return ReturnType.Integer;
                case "real": return ReturnType.Real;
                case "boolean": return ReturnType.Boolean;
                case "string": return ReturnType.String;
                default: throw new TypeException("unsupported type");
            }
        }
        public static string ToString(ReturnType type)
        {
            switch (type)
            {
                case ReturnType.Integer: return "integer";
                case ReturnType.Real: return "real";
                case ReturnType.Boolean: return "boolean";
                case ReturnType.String: return "string";
                default:
                    throw new TypeException("invalid ParseTypeBack");
            }
        }
        public static ReturnType FromLiteral(AST literal)
        {
            switch (literal.Token.Type)
            {
                case TokenType.IntegerLiteral: return ReturnType.Integer;
                case TokenType.RealLiteral: return ReturnType.Real;
                case TokenType.StringLiteral: return ReturnType.String;
                case TokenType.BooleanLiteral: return ReturnType.Boolean;
                default: throw new TypeException("Unsupported literal");
            }
        }
    }
}
