﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FormulaInterpreter.Core
{
    public enum ReturnType
    {
        Integer, Real, Boolean, String, Void
    }
    public class DST
    {
        public Rule Rule { get; protected set; }
        public Token Token { get; protected set; }
        public List<DST> Nodes { get; protected set; }
        public ReturnType ReturnType
        {
            get;  protected set;
        }
        public DST(Rule rule, ReturnType returnType, Token token, params DST[] nodes)
        {
            Rule = rule;
            Token = token;
            ReturnType = returnType;
            Nodes = new List<DST>(nodes);
        }
    }
}
