﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FormulaInterpreter.Core
{

    [Serializable]
    public class SemanterException:InterpreterException
    {
        public SemanterException() { }
        public SemanterException(string message) : base(message) { }
        public SemanterException(string message, Exception inner) : base(message, inner) { }
        protected SemanterException(
          System.Runtime.Serialization.SerializationInfo info,
          System.Runtime.Serialization.StreamingContext context) : base(info, context) { }
    }
    [Serializable]
    public class UndeclaredIdentifierException : SemanterException
    {
        public UndeclaredIdentifierException() { }
        public UndeclaredIdentifierException(string message) : base(message) { }
        public UndeclaredIdentifierException(string message, Exception inner) : base(message, inner) { }
        protected UndeclaredIdentifierException(
          System.Runtime.Serialization.SerializationInfo info,
          System.Runtime.Serialization.StreamingContext context) : base(info, context) { }
    }
    [Serializable]
    public class MultipleDeclarationException : SemanterException
    {
        public MultipleDeclarationException() { }
        public MultipleDeclarationException(string message) : base(message) { }
        public MultipleDeclarationException(string message, Exception inner) : base(message, inner) { }
        protected MultipleDeclarationException(
          System.Runtime.Serialization.SerializationInfo info,
          System.Runtime.Serialization.StreamingContext context) : base(info, context) { }
    }

    [Serializable]
    public class TypeException : SemanterException
    {
        public TypeException() { }
        public TypeException(string message) : base(message) { }
        public TypeException(string message, Exception inner) : base(message, inner) { }
        protected TypeException(
          System.Runtime.Serialization.SerializationInfo info,
          System.Runtime.Serialization.StreamingContext context) : base(info, context) { }
    }
    [Serializable]
    public class ImplicitConversionException : SemanterException
    {
        public ImplicitConversionException() { }
        public ImplicitConversionException(string message) : base(message) { }
        public ImplicitConversionException(string message, Exception inner) : base(message, inner) { }
        protected ImplicitConversionException(
          System.Runtime.Serialization.SerializationInfo info,
          System.Runtime.Serialization.StreamingContext context) : base(info, context) { }
    }

    [Serializable]
    public class UnsupportedOperationException : SemanterException
    {
        public UnsupportedOperationException() { }
        public UnsupportedOperationException(string message) : base(message) { }
        public UnsupportedOperationException(string message, Exception inner) : base(message, inner) { }
        protected UnsupportedOperationException(
          System.Runtime.Serialization.SerializationInfo info,
          System.Runtime.Serialization.StreamingContext context) : base(info, context) { }
    }

    [Serializable]
    public class VariableException : SemanterException
    {
        public VariableException() { }
        public VariableException(string message) : base(message) { }
        public VariableException(string message, Exception inner) : base(message, inner) { }
        protected VariableException(
          System.Runtime.Serialization.SerializationInfo info,
          System.Runtime.Serialization.StreamingContext context) : base(info, context) { }
    }

    [Serializable]
    public class FunctionException : SemanterException
    {
        public FunctionException() { }
        public FunctionException(string message) : base(message) { }
        public FunctionException(string message, Exception inner) : base(message, inner) { }
        protected FunctionException(
          System.Runtime.Serialization.SerializationInfo info,
          System.Runtime.Serialization.StreamingContext context) : base(info, context) { }
    }
}
