﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//TODO add custom exceptions with hierarchy
namespace FormulaInterpreter.Core
{
    [Serializable]
    public class UnsupportedCommandException : Exception
    {
        public UnsupportedCommandException() { }
        public UnsupportedCommandException(string message) : base(message) { }
        public UnsupportedCommandException(string message, Exception inner) : base(message, inner) { }
        protected UnsupportedCommandException(
          System.Runtime.Serialization.SerializationInfo info,
          System.Runtime.Serialization.StreamingContext context) : base(info, context) { }
    }
    //identifiers are writen with null in scope(so they can be seen in inner scope,
    //but cannot be used be declaration
    //after declaration was analysed null is changed to object
    //after assignment or in was called variable is initialised
        delegate void RuleAnalysis(Scope scope, AST rule);
    public static class Semanter
    {
        public static Scope Analyze(List<AST> commands)
        {
            Scope result = new Scope();
            Scope(result, commands);
            return result;
        }
        private static void Scope(Scope scope, List<AST> commands)
        {
            if (commands == null) return;
            List<DST> nodes = new List<DST>();
            var ASTDeclarations = commands.Where((command) => command.Rule == Rule.VariableDeclaration
            || command.Rule == Rule.FunctionDeclaration);
            foreach (var ASTDeclaration in ASTDeclarations)
            {
                if (ASTDeclaration.Rule == Rule.FunctionDeclaration)
                {
                    //just add names, not values
                    scope.Functions.Add(ASTDeclaration.Nodes[0].Token.Lexem, null);
                }
                else
                {
                    foreach (var variableNode in ASTDeclaration.Nodes)
                    {
                        if (variableNode.Rule == Rule.Variable)
                            scope.Variables.Add(variableNode.Token.Lexem, null);
                        else scope.Variables.Add(variableNode.Token.Lexem, null);
                    }
                }
            }
            Dictionary<Rule, RuleAnalysis> ruleMap = new Dictionary<Rule, RuleAnalysis>
            {
                { Rule.FunctionDeclaration, FunctionDeclaration },
                { Rule.VariableDeclaration, VariableDeclaration },
                { Rule.Assignment, Assignment},
                { Rule.MultiCommand, MultiCommand },
                { Rule.In, In },
                {Rule.Out, Out },
                { Rule.If, IfElse },
                { Rule.IfElse, IfElse },
                {Rule.While, While},
            };
            foreach (AST command in commands)
            {
                if (!ruleMap.ContainsKey(command.Rule)) throw new UnsupportedCommandException();
                ruleMap[command.Rule](scope, command);
            }
        }
        
        private static void FunctionDeclaration(Scope scope, AST rule)
        {
            Dictionary<string, VariableStatus> parameters = new Dictionary<string, VariableStatus>();
            for (int i = 1; i < rule.Nodes.Count - 1; i++)
            {
                parameters.Add(rule.Nodes[i].Nodes[0].Token.Lexem,
                    new VariableStatus(ReturnTypeConverter.FromString(rule.Nodes[i].Token.Lexem), true));
            }
            FunctionScope functionScope = new FunctionScope(ReturnTypeConverter.FromString(rule.Token.Lexem),scope, parameteres: parameters);
            scope.Functions.Set(rule.Nodes[0].Token.Lexem, functionScope);
            Scope(functionScope, rule.Nodes.Last().Nodes);
            if (!functionScope.Result.Initialised) throw new Exception("result must be assigned with value");
        }
        private static void VariableDeclaration(Scope scope, AST rule)
        {
            ReturnType type = ReturnTypeConverter.FromString(rule.Token.Lexem);
            foreach (var node in rule.Nodes)
            {
                if (node.Rule == Rule.Variable)
                {
                    scope.Variables.Set(node.Token.Lexem, new VariableStatus(type));
                }
                else
                {
                    scope.Variables.Set(node.Token.Lexem, new VariableStatus(type));
                    Assignment(scope, node);
                }
            }
        }
        private static void Assignment(Scope scope, AST rule)
        {
            var variable = scope.Variables.Get(rule.Token.Lexem);
            DST value = ExpressionAnalyzer.OfType(scope, rule.Nodes[0],variable.Type);
            scope.Nodes.Add(new DST(Rule.Assignment, ReturnType.Void, rule.Token,value));
            variable.Initialised = true;
        }
        private static void MultiCommand(Scope scope, AST rule)
        {
            Scope result = new Scope(scope);
            Scope(result, rule.Nodes);
            scope.Nodes.Add(result);
        }
        private static void In(Scope scope, AST rule)
        {
            var identifiers = new List<DST>();
            foreach(var identifier in rule.Nodes)
            {
                var variable = scope.Variables.Get(identifier.Token.Lexem);
                identifiers.Add(new DST(Rule.Variable,
                    variable.Type, identifier.Token));
                variable.Initialised = true;
            }
            scope.Nodes.Add(
                new DST(Rule.In, ReturnType.Void, rule.Token,identifiers.ToArray()));
        }
        private static void Out(Scope scope, AST rule)
        {
            var parameters = new List<DST>();
            foreach(var parameter in rule.Nodes)
            {
                parameters.Add(ExpressionAnalyzer.OfType(scope, parameter, ReturnType.String));
            }
            scope.Nodes.Add(new DST(Rule.Out, ReturnType.Void, rule.Token, parameters.ToArray()));
        }
        private static Dictionary<string,VariableStatus> GetUnintialisedVariables(this Scope scope)
        {
            return scope.Variables.AllDeclarations.Where((kv) => !kv.Value?.Initialised ?? false).
                ToDictionary((kv) => kv.Key, (kv) => kv.Value);
        }
        private static Dictionary<string, VariableStatus> CancelInitialisation
            (this Scope scope, Dictionary<string,VariableStatus> variables)
        {
            return scope.Variables.AllDeclarations.Where((kv) =>
            {
                if ((kv.Value?.Initialised ?? false) && variables.ContainsKey(kv.Key))
                {
                    scope.Variables.Get(kv.Key).Initialised = false;
                    return true;
                }
                return false;
            }).
                ToDictionary((kv) => kv.Key, (kv) => kv.Value);
        }
        private static void IfElse(Scope scope, AST rule)
        {
            //saving unitialisedVaraibles
            var unInitialisedVariables = scope.GetUnintialisedVariables();
            var condition = ExpressionAnalyzer.OfType(scope, rule.Nodes[0], ReturnType.Boolean);
            var positive = new Scope(scope);
                Scope(positive,rule.Nodes[1].Nodes);
            var result = new DST(rule.Rule, ReturnType.Void, rule.Token, condition, positive);
            //make variables initialised in if uninitialised again
            var initialisedInIf = scope.CancelInitialisation(unInitialisedVariables);
            if(rule.Rule == Rule.IfElse)
            {
                var negative = new Scope(scope);
                Scope(negative, rule.Nodes[2].Nodes);
                result.Nodes.Add(negative);
            }
            var InitialisedInElse = scope.CancelInitialisation(unInitialisedVariables);
            var InitialisedInBoth = initialisedInIf.Intersect(InitialisedInElse);
            foreach(var trulyInitialised in InitialisedInBoth)
            {
                scope.Variables.Get(trulyInitialised.Key).Initialised = true;
            }
            scope.Nodes.Add(result);
        }
        private static void While(Scope scope, AST rule)
        {
            //saving unitialisedVaraibles
            var unInitialisedVariables = scope.GetUnintialisedVariables();
            var condition = ExpressionAnalyzer.OfType(scope, rule.Nodes[0], ReturnType.Boolean);
            var body = new Scope(scope);
            Scope(body, rule.Nodes[1].Nodes);
            var result = new DST(rule.Rule, ReturnType.Void, rule.Token, condition, body);
            //make variables initialised in if uninitialised again
            scope.CancelInitialisation(unInitialisedVariables);
            scope.Nodes.Add(result);
        }
    }
}
